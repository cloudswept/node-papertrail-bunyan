/*
 * Copyright (C) Cloudswept Ltd - All Rights Reserved
 */
const Bunyan = require('bunyan'),
  BunyanSyslog = require('bunyan-syslog'),
  OS = require('os');

function createLogger(name, hostname, enablePapertrail, papertrailHost, papertrailPort, level = "debug") {
  const streams = [
    {
      level,
      stream: process.stdout
    }
  ];

  if (enablePapertrail && papertrailHost && papertrailPort) {
    const output = BunyanSyslog.createBunyanStream({
      name,
      type: "tcp",
      facility: BunyanSyslog.local0,
      host: papertrailHost,
      port: +papertrailPort,
      format: (message) => {
        const levelName = Bunyan.nameFromLevel[message.level].toUpperCase();
        return `${levelName} ${message.msg}`;
      }
    });
    streams.push({
      level,
      type: "raw",
      stream: output
    });
  }

  return Bunyan.createLogger({
    name,
    hostname,
    streams
  });
};

function createAWSLogger(name, hostname, level = undefined) {
  return createLogger(
    process.env.AWS_LAMBDA_FUNCTION_NAME || name || "node",
    hostname || OS.hostname(),
    !!process.env.AWS_LAMBDA_FUNCTION_NAME,
    process.env.PAPERTRAIL_HOST,
    process.env.PAPERTRAIL_PORT,
    level || process.env.PAPERTRAIL_LEVEL || "debug"
  );
}

module.exports = createLogger;
module.exports.createAWSLogger = createAWSLogger;
